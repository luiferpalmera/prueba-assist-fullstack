# prueba-assist-fullstack

## Aplicación Front
Angular CLI: 14.1.2
Node: 14.15.5
Al clonar la aplicación, ejecutar la instrucción npm install. Así mismo, para correr la aplicación con el comando ng s.

## Aplicación Back
.NetCore 5
Para las reglas de validaciones, se usó FluentValidation
Para la configuración de nivel de datos, en el archivo appsettings.json se debe modificar lo siguiente
"ConnectionStrings": {
  "DefaultConexion": "Data Source=ins-dllo-test-01.public.33e082952ab4.database.windows.net,3342;Initial Catalog=TestDB; User ID=prueba;Password=pruebaconcepto;"
}
