﻿namespace Application.Common.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task AddAsync(TEntity item);
        Task DeleteAsync(int id);
        Task<TEntity?> GetByIdAsync(object id);
        Task<IEnumerable<TEntity>> GetAllAsync();

        IQueryable<TEntity> NoTracking();

        Task SaveChangesAsync();
    }
}
