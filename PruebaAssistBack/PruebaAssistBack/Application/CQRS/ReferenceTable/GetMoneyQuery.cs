﻿using Application.Common.Interfaces;
using Application.DTOs;
using AutoMapper;
using Domain.Models.TablasReferencia;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.CQRS.ReferenceTable
{
    public class GetMoneyQuery : IRequest<ResponseDto> { }

    public class GetMoneyQueryHandler : IRequestHandler<GetMoneyQuery, ResponseDto>
    {
        private readonly IRepository<Money> _repository;
        private readonly IMapper _mapper;

        public GetMoneyQueryHandler(IRepository<Money> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<ResponseDto> Handle(GetMoneyQuery request, CancellationToken cancellationToken)
        {
            var list = await _repository.NoTracking()
                    .ToListAsync();

            return new ResponseDto(true, list);

        }

    }

}
