﻿using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.DTOs;
using Domain.Models;
using MediatR;

namespace Application.CQRS.BranchOffices.Commands.Update
{
    public class UpdateBranchOfficeCommand : IRequest<ResponseDto>
    {
        public int Id { get; set; } = default!;
        public int Code { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string Identification { get; set; }
        public DateTime? CreateDate { get; set; }
        public int MoneyId { get; set; }
    }

    public class UpdateBranchOfficeCommandHandler : IRequestHandler<UpdateBranchOfficeCommand, ResponseDto>
    {

        private readonly IRepository<BranchOffice> _repository;

        public UpdateBranchOfficeCommandHandler(IRepository<BranchOffice> repository)
        {
            _repository = repository;
        }


        async Task<ResponseDto> IRequestHandler<UpdateBranchOfficeCommand, ResponseDto>
            .Handle(UpdateBranchOfficeCommand request, CancellationToken cancellationToken)
        {

            var branchOffice = await _repository.GetByIdAsync(request.Id);

            if (branchOffice == null)
                throw new NotFoundException(nameof(BranchOffice), request.Id);

            branchOffice.Code = request.Code;
            branchOffice.Description = request.Description;
            branchOffice.Address = request.Address;
            branchOffice.Identification = request.Identification;
            branchOffice.CreateDate = request.CreateDate;
            branchOffice.MoneyId = request.MoneyId;

            await _repository.SaveChangesAsync();

            return new ResponseDto(true, branchOffice.Id);
        }

    }
}
