﻿using Application.Common.Interfaces;
using Application.DTOs;
using Domain.Models;
using MediatR;

namespace Application.CQRS.BranchOffices.Commands.Create
{
    public class CreateBranchOfficeCommand : IRequest<ResponseDto>
    {
        public int? Id { get; set; }
        public int Code { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string Identification { get; set; }
        public DateTime? CreateDate { get; set; }
        public int MoneyId { get; set; }
    }

    public class CreateBranchOfficeCommandHandler : IRequestHandler<CreateBranchOfficeCommand, ResponseDto>
    {
        private readonly IRepository<BranchOffice> _repository;

        public CreateBranchOfficeCommandHandler(IRepository<BranchOffice> repository)
        {
            _repository = repository;
        }


        async Task<ResponseDto> IRequestHandler<CreateBranchOfficeCommand, ResponseDto>
            .Handle(CreateBranchOfficeCommand request, CancellationToken cancellationToken)
        {
            var newBranchOffice = new BranchOffice
            {
                Code = request.Code,
                Description = request.Description,
                Address = request.Address,
                Identification = request.Identification,
                CreateDate = request.CreateDate,
                MoneyId = request.MoneyId
            };

            await _repository.AddAsync(newBranchOffice);
            await _repository.SaveChangesAsync();

            return new ResponseDto(true, request.Code);
        }

    }
}
