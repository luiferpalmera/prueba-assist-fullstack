﻿using FluentValidation;

namespace Application.CQRS.BranchOffices.Commands.Create
{
    public class CreateBranchOfficeValidator : AbstractValidator<CreateBranchOfficeCommand>
    {
        public CreateBranchOfficeValidator()
        {
            RuleFor(r => r.Description).NotNull();
            RuleFor(r => r.Description).Length(1, 250);

            RuleFor(r => r.Address).NotNull();
            RuleFor(r => r.Address).Length(1, 250);

            RuleFor(r => r.Identification).NotNull();
            RuleFor(r => r.Identification).Length(1, 50);

            RuleFor(r => r.MoneyId).NotNull();

            DateTime currentDate = DateTime.Now;
            RuleFor(r => r.CreateDate).NotNull();
            RuleFor(x => x.CreateDate).GreaterThanOrEqualTo(currentDate);
        }
    }
}
