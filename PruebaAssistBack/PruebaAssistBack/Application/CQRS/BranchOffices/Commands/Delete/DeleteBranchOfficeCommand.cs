﻿using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.DTOs;
using Domain.Models;
using MediatR;

namespace Application.CQRS.BranchOffices.Commands.Delete
{
    public class DeleteBranchOfficeCommand : IRequest<ResponseDto>
    {
        public int Id { get; set; }
    }

    public class DeleteBranchOfficeCommandHandler : IRequestHandler<DeleteBranchOfficeCommand, ResponseDto>
    {
        private readonly IRepository<BranchOffice> _repository;

        public DeleteBranchOfficeCommandHandler(IRepository<Domain.Models.BranchOffice> repository)
        {
            _repository = repository;
        }


        async Task<ResponseDto> IRequestHandler<DeleteBranchOfficeCommand, ResponseDto>
            .Handle(DeleteBranchOfficeCommand request, CancellationToken cancellationToken)
        {

            var branchOffice = await _repository.GetByIdAsync(request.Id);

            if (branchOffice == null)
                throw new NotFoundException(nameof(BranchOffice), request.Id);

            await _repository.DeleteAsync(request.Id);
            await _repository.SaveChangesAsync();

            return new ResponseDto(true, request.Id);
        }

    }
}

