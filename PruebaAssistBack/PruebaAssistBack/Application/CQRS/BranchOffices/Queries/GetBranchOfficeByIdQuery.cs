﻿using Application.Common.Interfaces;
using Application.DTOs;
using AutoMapper;
using Domain.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.CQRS.BranchOffices.Queries
{
    public class GetBranchOfficeByIdQuery : IRequest<ResponseDto>
    {
        public int Id { get; set; }
    }

    public class GetBranchOfficeByIdQueryHandler : IRequestHandler<GetBranchOfficeByIdQuery, ResponseDto>
    {
        private readonly IRepository<BranchOffice> _repository;
        private readonly IMapper _mapper;

        public GetBranchOfficeByIdQueryHandler(IRepository<BranchOffice> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<ResponseDto> Handle(GetBranchOfficeByIdQuery request, CancellationToken cancellationToken)
        {
            var branchOffice = await _repository.NoTracking().Include(x => x.Money)
                .Where(x=> x.Id == request.Id)
                .FirstOrDefaultAsync() ;

            return new ResponseDto(true, branchOffice);

        }

    }

}
