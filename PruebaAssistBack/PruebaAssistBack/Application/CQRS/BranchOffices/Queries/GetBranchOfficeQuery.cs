﻿using Application.Common.Interfaces;
using Application.DTOs;
using AutoMapper;
using Domain.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.CQRS.BranchOffices.Queries
{
    public class GetBranchOfficeQuery : IRequest<ResponseDto>
    {
    }

    public class GetBranchOfficeQueryHandler : IRequestHandler<GetBranchOfficeQuery, ResponseDto>
    {
        private readonly IRepository<Domain.Models.BranchOffice> _repository;
        private readonly IMapper _mapper;

        public GetBranchOfficeQueryHandler(IRepository<BranchOffice> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<ResponseDto> Handle(GetBranchOfficeQuery request, CancellationToken cancellationToken)
        {
            var branchOffices = await _repository.NoTracking()
                .Include(x=> x.Money)
                .ToListAsync();

            return new ResponseDto(true, branchOffices);

        }

    }
}
