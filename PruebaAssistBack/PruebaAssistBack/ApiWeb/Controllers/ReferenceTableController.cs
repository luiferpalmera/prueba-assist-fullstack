﻿using Application.CQRS.ReferenceTable;
using Application.DTOs;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace ApiWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReferenceTableController : Controller
    {
        private readonly IMediator _mediator;

        public ReferenceTableController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("Money")]
        public Task<ResponseDto> Moneies([FromRoute] GetMoneyQuery query)
                => _mediator.Send(query);

    }

}
