﻿using Application.CQRS.BranchOffices.Commands.Create;
using Application.CQRS.BranchOffices.Commands.Delete;
using Application.CQRS.BranchOffices.Commands.Update;
using Application.CQRS.BranchOffices.Queries;
using Application.DTOs;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace ApiWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BranchOfficeController : ControllerBase
    {
        private readonly IMediator _mediator;

        public BranchOfficeController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public Task<ResponseDto> Get([FromQuery] GetBranchOfficeQuery query)
                => _mediator.Send(query);

        [HttpGet("{Id}")]
        public Task<ResponseDto> Get([FromRoute] GetBranchOfficeByIdQuery query)
                => _mediator.Send(query);



        [HttpPost]
        public Task<ResponseDto> Create([FromBody] CreateBranchOfficeCommand command)
                => _mediator.Send(command);

        [HttpPut]
        public Task<ResponseDto> Update([FromBody] UpdateBranchOfficeCommand command)
                => _mediator.Send(command);

        [HttpDelete("{Id}")]
        public Task<ResponseDto> Delete([FromRoute] DeleteBranchOfficeCommand command)
                => _mediator.Send(command);

    }
}
