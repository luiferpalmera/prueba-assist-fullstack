﻿using Domain.Models.TablasReferencia;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models
{
    public class BranchOffice
    {
        public int? Id { get; set; }
        public int Code { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string Identification { get; set; }
        public DateTime? CreateDate { get; set; }
        public int MoneyId { get; set; }

        [ForeignKey(nameof(MoneyId))]
        public Money Money { get; set; }
    }
}
