import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BranchOfficeComponent } from './components/branch-office/branch-office.component';
import { SaveBranchOfficeComponent } from './components/save-branch-office/save-branch-office.component';

const routes: Routes = [
  { path: 'sucursales', component: BranchOfficeComponent },
  { path: 'guardar-sucursal', component: SaveBranchOfficeComponent },
  { path: 'guardar-sucursal/:id', component: SaveBranchOfficeComponent },
  { path: '**', pathMatch: 'full', redirectTo:'/sucursales' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
