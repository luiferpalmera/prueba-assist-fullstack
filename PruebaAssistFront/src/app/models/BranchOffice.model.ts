import { Money } from "./Money.model";

export interface BranchOffice{
  Id?:number;
  Code:number;
  Description:string;
  Address:string;
  Identification:string;
  CreateDate:string;
  Money:Money;
}
