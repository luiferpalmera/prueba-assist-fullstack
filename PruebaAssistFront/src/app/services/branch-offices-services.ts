import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { BranchOffice } from '../models/BranchOffice.model';

@Injectable({
  providedIn: 'root'
})
export class BranchOfficeService {

  headers = { headers: new HttpHeaders({})};


  constructor(private httpClient:HttpClient) { }

    getBranchOffices(): Observable<any>{
        return this.httpClient.get(environment.api+'BranchOffice')
    }
    saveBranchOffice(branchOffice : BranchOffice): Observable<any>{
      return this.httpClient.post(environment.api+'BranchOffice',branchOffice);
    }
    updateBranchOffice(branchOffice : BranchOffice): Observable<any>{
      return this.httpClient.put(environment.api+'BranchOffice',branchOffice);
    }
    deleteBranchOffice(id:number | undefined): Observable<any>{
      return this.httpClient.delete(environment.api+'BranchOffice/'+id);
    }
    getDataReferencia(): Observable<any>{
        return this.httpClient.get(environment.api+'ReferenceTable/Money')
    }
    getBranchOfficeById(id:number): Observable<any>{
        return this.httpClient.get(environment.api+'BranchOffice/'+id)
    }

}
