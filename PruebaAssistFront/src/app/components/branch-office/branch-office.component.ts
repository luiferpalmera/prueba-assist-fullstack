import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { BranchOffice } from 'src/app/models/BranchOffice.model';
import { BranchOfficeService } from 'src/app/services/branch-offices-services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-branch-office',
  templateUrl: './branch-office.component.html',
  styleUrls: ['./branch-office.component.css']
})
export class BranchOfficeComponent implements OnInit {

  errores:object[]=[];
  branchOffices:BranchOffice[] = [];

  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering


  constructor(private _branchOfficeService:BranchOfficeService) {

  }

  ngOnInit(): void {
    this._branchOfficeService.getBranchOffices().subscribe(data => {
      this.branchOffices = data.Data;
    });

  }

  eliminarEmpleado(branchOffice:BranchOffice){

  }

  mostrarConfirmacionEliminar(branchOffice:BranchOffice, index: number){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Seguro que desea eliminar el registro?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, borrarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this._branchOfficeService.deleteBranchOffice(branchOffice.Id).subscribe(data => {
          this.branchOffices.splice(index,1);
          swalWithBootstrapButtons.fire(
            'Borrado!',
            'El registro ha sido eliminado con éxito.',
            'success'
          )

        });

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          'Your imaginary file is safe :)',
          'error'
        )
      }
    })
  }

}
