import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Money } from 'src/app/models/Money.model';
import { BranchOfficeService } from 'src/app/services/branch-offices-services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-save-branch-office',
  templateUrl: './save-branch-office.component.html',
  styleUrls: ['./save-branch-office.component.css']
})
export class SaveBranchOfficeComponent implements OnInit {
  public formBranchOffice: FormGroup ;
  errores:object[]=[];
  moneies:Money[] = [];
  public fechaActual:Date;
  public minDate:Date;
  public id:string;
  constructor(private _branchOfficeService:BranchOfficeService, private _formBuilder: FormBuilder, private rout: ActivatedRoute) {
    this.formBranchOffice = this._formBuilder.group({
      id: [],
      code : ['', [Validators.required, Validators.pattern('^[0-9]+')]],
      description: ['', [Validators.required, Validators.maxLength(250)]],
      address: ['', [Validators.required, Validators.maxLength(250)]],
      identification: ['', [Validators.required, Validators.maxLength(50)]],
      createDate: [null,[Validators.required]],
      moneyId: [,[Validators.required]]
    });
    this.fechaActual = new Date();
    this.minDate = new Date();
    this.id = '';
  }

  ngOnInit(): void {
    this.construirDataReferencia();
    this.id = this.rout.snapshot.params['id'];

    if(this.id !== ''){
      this._branchOfficeService.getBranchOfficeById(parseInt(this.id)).subscribe(data => {
        let dataInfo = data.Data;
        for ( const atributos  in this.formBranchOffice.value) {
          if(atributos === 'createDate'){
            dataInfo.CreateDate = new Date(dataInfo.CreateDate);
          }
          this.formBranchOffice.get(atributos)?.setValue(dataInfo[this.mapAttributes(atributos)]);
        }
      });
    }
  }
  mapAttributes(str: any){

    return str.charAt(0).toUpperCase() + str.slice(1);
  }
  construirDataReferencia(){
    this._branchOfficeService.getDataReferencia().subscribe(data => {
      this.moneies = data.Data;
    });

  }

  saveBranchOffice(){
    if( !this.validateForm() ){ return; }
    if(this.id != null && this.id != ''){
      this.updateBranchOffice();
    }else{
      this.saveBranchOfficeMethod();
    }
  }

  updateBranchOffice(){
    this.formBranchOffice.get('id')?.setValue(this.id);
      this._branchOfficeService.updateBranchOffice(this.formBranchOffice.value).subscribe(data => {
        console.log(data);
        if(data.Status){
          Swal.fire({
            title: 'Realizado!',
            text: 'Acción realizada satisfactoriamente.',
            icon: 'success',
            timer: 2000,
            confirmButtonText: 'Ok'
          })

          setTimeout(() => {
            window.location.href = "/sucursales"
          }, 2000);
        }else{
          this.errores = data.errors;
        }
      }),(error : any)=> this.errores = error.errors;

      Swal.fire({
        title: 'Error!',
        text: 'No se pudo efectuar la operación. Intente más tarde.',
        icon: 'error',
        confirmButtonText: 'Ok'
      });
  }
  saveBranchOfficeMethod(){
      this._branchOfficeService.saveBranchOffice(this.formBranchOffice.value).subscribe(data => {
        console.log(data);
        if(data.Status){
          Swal.fire({
            title: 'Realizado!',
            text: 'Acción realizada satisfactoriamente.',
            icon: 'success',
            timer: 2000,
            confirmButtonText: 'Ok'
          })

          setTimeout(() => {
            window.location.href = "/sucursales"
          }, 2000);
        }else{
          this.errores = data.errors;
        }
      }),(error : any)=> this.errores = error.errors;

      Swal.fire({
        title: 'Error!',
        text: 'No se pudo efectuar la operación. Intente más tarde.',
        icon: 'error',
        confirmButtonText: 'Ok'
      });
  }
  validateForm(){
    if (this.formBranchOffice.invalid) {


      Object.values(this.formBranchOffice.controls).forEach(control => {

        if (control instanceof FormGroup) {
          Object.values(control.controls).forEach(control => control.markAsTouched());
        } else {
          control.markAsTouched();
        }


      });
      return false;
    }
    return true;
  }

}
